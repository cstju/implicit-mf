# -*- coding: utf-8 -*-
# @Author: 3dlabuser
# @Date:   2016-07-07 21:59:16
# @Last Modified by:   mac@lab538
# @Last Modified time: 2016-07-17 15:49:32
import numpy as np
import pandas as pd
import scipy.sparse as sparse
import time
import pdb
from scipy.sparse.linalg import spsolve
from sklearn import cross_validation as cv
from sklearn.metrics.pairwise import pairwise_distances
from sklearn.metrics import mean_squared_error
from math import sqrt
from operator import itemgetter

data_file = "/Users/3dlabuser/Documents/datasets/MovieLens/MovieLens 1M Dataset/ml-1m/ratings.dat"
parameter_file = "/Users/3dlabuser/Documents/python_working_directory/recsys/recsys_learn/implicit-mf-master_parameter_result/parameter_result_testset.csv"
parameter_file2 = "/Users/3dlabuser/Documents/python_working_directory/recsys/recsys_learn/implicit-mf-master_parameter_result/parameter_result_trainset.csv"
class ImplicitMF():

	def __init__(self,num_factors=30,num_iterations=15,reg_lambda=0.0,alpha=0.0,theta=0.1,K=20):

		self.num_factors = num_factors#特征个数
		self.num_iterations = num_iterations#迭代次数
		self.reg_lambda = reg_lambda#正则化系数
		self.theta = theta
		self.alpha = alpha#cui = 1 + alpha * log(1+rui/theta)
		self.K = K#预测结果取前K个

	def ReadData(self,filename):

		header  = ['user_id', 'item_id','rating','timestamp']
		df = pd.read_csv(filename, sep = '::', names = header, engine = 'python')

		"""
		由于用户和物品的id都是从1开始的连续整数
		所以读取user_id和item_id的最大值为对应的个数
		"""
		self.num_users = pd.Series.max(df.user_id)
		self.num_items = pd.Series.max(df.item_id)

		"""
		#采用这种方法读出来的物品个数为3706，代表有3706个物品被评过分
		#实际上应该是3952，代表实际上物品总数是3952
		#原因有可能是有的物品没有被评过分，但依旧收录在物品列表中
		self.num_users = df.user_id.unique().shape[0]#统计用户数
		self.num_items = df.item_id.unique().shape[0]#统计物品数
		"""

		print "用户总数为："+str(self.num_users)
		print "物品总数为："+str(self.num_items)

		#pdb.set_trace()

		self.train_data,self.test_data = cv.train_test_split(df,test_size = 0.25)

		self.train_data_matrix = np.zeros((self.num_users,self.num_items))
		for line in self.train_data.itertuples():
			self.train_data_matrix[line[1]-1, line[2]-1] = line[3]#这里构建的矩阵中的值是1-5的评分

		self.test_data_matrix = np.zeros((self.num_users,self.num_items))
		for line in self.test_data.itertuples():
			self.test_data_matrix[line[1]-1, line[2]-1] = line[3]#这里构建的矩阵中的值是1-5的评分

		print "。。。。。构建用户-物品初始训练集和测试集完成。。。。。"

		self.train_bool_matrix = self.train_data_matrix.copy()
		self.train_bool_matrix[self.train_data_matrix>0] = 1.0 #将训练矩阵中的值转化为0-1，0代表没有发生过行为，1代表发生过行为
		self.test_bool_matrix = self.test_data_matrix.copy()
		self.test_bool_matrix[self.test_data_matrix>0] = 1.0
		print "。。。。。构建用户-物品二值训练矩阵和测试矩阵完成。。。。。"

		# self.train_confident_matrix = self.train_data_matrix.copy()#以原始的评分作为置信度
		# self.test_confident_matrix = self.test_data_matrix.copy()
		# print "。。。。。构建用户-物品置信度训练矩阵和测试矩阵完成。。。。。"
	def train_parameter(self,filename):
		min_reg_lambda = 0
		min_alpha = 0
		max_reg_lambda = 10
		max_alpha = 20
		parameter_result = file(filename,'w')
		for reg_lambda in range(min_reg_lambda,max_reg_lambda):
			for alpha in range(min_alpha,max_alpha):
				rmse,rank = self.train_model(reg_lambda,alpha)
				print "正则化参数为:%f,置信度系数为:%f时，迭代15次得到的rmse为:%f,平均排名为:%f。" %(reg_lambda,alpha,rmse,rank)
				rmse_test,rank_test = self.predict()
				print "正则化参数为:%f,置信度系数为:%f时，迭代15次测试集的rmse为:%f,平均排名为:%f。" %(reg_lambda,alpha,rmse_test,rank_test)
				parameter_result.write("reg_lambda="+str(reg_lambda)+"\talpha="+str(alpha)+"\trmse="+str(rmse_test)+"\trank="+str(rank_test))
				parameter_result.write("\r\n")
		parameter_result.close()



	def train_model(self,reg_lambda,alpha):
		self.user_vectors = np.random.normal(size=(self.num_users,self.num_factors))
		self.item_vectors = np.random.normal(size=(self.num_items,self.num_factors))
		self.reg_lambda = reg_lambda
		self.alpha = alpha
		parameter_result2 = file(parameter_file2,'a')
		for i in range(self.num_iterations):
			t0 = time.time()
			print "~~@@~~@@~~@@~~@@~~@@~~@@~~@@~~@@~~@@~~@@~~@@~~@@~~@@" 
			print "正则化参数为:%f" %self.reg_lambda
			print "置信度系数为:%f" %self.alpha
			print "。。。。。第%i次迭代计算开始。。。。。" %(i+1)
			print "。。。。。开始计算用户特征向量。。。。。"
			
			self.user_vectors = self.iteration(True,self.item_vectors)
			print "。。。。。开始计算物品特征向量。。。。。"
			self.item_vectors = self.iteration(False,self.user_vectors)
			t1 = time.time()
			print "。。。。。第%i次迭代计算完成,耗时%f秒。。。。。" %(i+1,t1-t0)
			
			prediction = self.user_vectors.dot(self.item_vectors.T)#每次训练都会计算一次结果
			rmse_i = self.rmse(prediction,self.train_bool_matrix)
			print "。。。。。第%i次迭代计算结果的rmse%f。。。。。" %(i+1,rmse_i)
			rank_i = self.average_rank(prediction,self.train_data_matrix)
			print "。。。。。第%i次迭代计算结果的平均排名%f。。。。。" %(i+1,rank_i)

			parameter_result2.write("reg_lambda="+str(reg_lambda)+"\talpha="+str(alpha)+"\titeration="+str(i)+"\trmse="+str(rmse_i)+"\trank="+str(rank_i))
			parameter_result2.write("\r\n")

		parameter_result2.close()
			
		return rmse_i,rank_i
		


	def predict(self):
		print "##$$##$$##$$##$$##$$##$$##$$##$$##$$##$$##$$"
		prediction = self.user_vectors.dot(self.item_vectors.T)
		rmse = self.rmse(prediction,self.test_bool_matrix)
		print "。。。。。测试集计算结果的rmse%f。。。。。" %rmse
		rank = self.average_rank(prediction,self.test_data_matrix)
		print "。。。。。测试集计算结果的平均排名%f。。。。。" %rank
		return rmse,rank

	def iteration(self,user,fixed_vecs):
		"""
		如果此次迭代是计算用户特征向量
		num_solve的值为用户数
		num_fixed的值为物品数
		如果此次迭代是计算物品特征向量
		num_solve的值为物品数
		num_fixed的值为用户数
		"""
		num_solve = self.num_users if user else self.num_items
		num_fixed = self.num_items if user else self.num_users
		Y = sparse.csr_matrix(fixed_vecs)#csr_matrix函数是一个针对稀疏矩阵快速运算的方法，从公式角度没有实际变化
		YTY = Y.T.dot(Y)
		eye = sparse.eye(num_fixed)
		lambda_eye = self.reg_lambda*sparse.eye(self.num_factors)
		solve_vecs = np.zeros((num_solve, self.num_factors))

		t = time.time()
		for i in range(num_solve):
			if user:
				"""
				论文中置信度有两中计算方式
				第一种:confident_i = 1+self.alpha*self.train_data_matrix[i]
				第二种:confident_i = 1.0+self.alpha*np.log(1.0+self.train_data_matrix[i]/self.theta)
				这里为了调参方便，先采用第一种置信度计算方式，论文中采用的是第二种计算方式
				"""
				confident_i = 1+self.alpha*self.train_data_matrix[i]
				#confident_i = 1.0+self.alpha*np.log(1.0+self.train_data_matrix[i]/self.theta)
				pu = self.train_bool_matrix[i]#pu为一个列向量q
			else:
				confident_i = 1+self.alpha*self.train_data_matrix[:,i].T
				#confident_i = 1.0+self.alpha*np.log(1.0+self.train_data_matrix[:,i].T/self.theta)
				pu = self.train_bool_matrix[:,i].T #pu为一个列向量
			CuI = sparse.diags(confident_i,0)-eye
			YTCuIY = Y.T.dot(CuI).dot(Y)
			YTCupu = Y.T.dot(CuI+eye).dot(sparse.csr_matrix(pu).T)
			# print "YTY的维度：" + str(YTY.shape)
			# print "YTCuIY的维度：" + str(YTCuIY.shape)
			# print "lambda_eye的维度：" + str(lambda_eye.shape)
			# print "YTCupu的维度：" + str(YTCupu.shape)
			xu = spsolve(YTY+YTCuIY+lambda_eye,YTCupu)
			# print "xu的维度：" + str(xu.shape)
			solve_vecs[i] = xu.T
			# print "solve_vecs[i]的维度：" + str(solve_vecs[i].shape)
			# print "solve_vecs的维度：" + str(solve_vecs.shape)
			# pdb.set_trace()
			if i % 1000 == 0:
				print '生成 %i 个 %s 特征向量，总耗时 %d 秒' % (i,'用户' if user else '物品',time.time() - t)
		return solve_vecs

	def rmse(self,prediction,ground_truth):#计算预测矩阵和真实矩阵之间的rmse
		prediction = prediction[ground_truth.nonzero()].flatten()#将2维预测矩阵转化为1维数组
		ground_truth = ground_truth[ground_truth.nonzero()].flatten()
		return sqrt(mean_squared_error(prediction,ground_truth))

	def average_rank(self,prediction,ground_truth):#计算预测矩阵的平均排名
		prediction_rank = np.zeros((self.num_users,1))
		for userid in range(self.num_users):
			rankid = 0.0
			prediction_useri = prediction[userid]#得到用户userid的预测值
			ground_truth_useri = ground_truth[userid]#用户userid的真实评分
			prediction_useri_dec = {}
			for itemid in range(len(ground_truth_useri)):
				if ground_truth_useri[itemid] != 0:
					prediction_useri_dec.setdefault(itemid,0)
					prediction_useri_dec[itemid] = prediction_useri[itemid]#得到用户真实评分中的非0元素所对应的预测值和商品号

			rank_num = 10 if len(prediction_useri_dec)>10 else len(prediction_useri_dec)
			for idd,pui in sorted(prediction_useri_dec.iteritems(),key = itemgetter(1),reverse=True)[:10]:#将预测分数中非0元素从大到小排序个(取前10个)
			#rankid/rank_num 是得到相对排名，取值在0-1
				prediction_rank[userid][0] += (rankid/rank_num) * ground_truth_useri[idd]
				rankid += 1.0
			prediction_rank[userid][0] = prediction_rank[userid][0]/np.sum(ground_truth_useri)

		return np.sum(prediction_rank)/self.num_users




if __name__ == '__main__':
	recsys = ImplicitMF()
	recsys.ReadData(data_file)
	recsys.train_parameter(parameter_file)
	#recsys.train_model(0,10)
	#recsys.predict()


