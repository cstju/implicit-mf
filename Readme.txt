说明：
（1）本程序是依照https://github.com/MrChrisJohnson/implicit-mf
进行的自我学习和更改。
论文为：Collaborative Filtering for Implicit Feedback Datasets
主要目的是为了学习隐式反馈的具体实现过程（重点是矩阵形式相乘直接出结果）

（2）采用的数据集是ml-1m，在论文中没有采用这个数据集，因为这个数据集是显性评分数据集
不适合用于这篇论文提出的算法。但是由于论文中的数据集不公开，所以本程序还是采用ml-1m

(3)
原论文中的使用的公式为了与ml-1m相对应这里做一些含义的解释：
（a）pui：pui为用户u对物品i是否产生行为，根据ml-1m的数据，
如果用户u观看过电影i则将pui设为1，用户u没有观看过电影i就pui为0。
最终需要预测的值也是pui
（b）rui：在原论文中rui为用户u对物品i产生的行为时间长度，
并且做了一个时间衰减：rui = rui*exp(-at-b)/(1+exp(-at-b))
其中t为用户换完台之后观看的第t个节目。
例如换台后观看的第三个节目，时间衰减乘的是0.5。换台后第5个节目，时间衰减乘的就是0.99。
根据ml-1m的情况，rui为用户u对电影i的真实评分。由于测试集和训练集的分配是随机分配的。
在分配的时候没有考虑到时间因素，所以目前先不增加时间衰减。

（4）
本代码需要调的参数:
num_factors:特征向量的个数
num_iterations：迭代次数
reg_lambda：正则化系数
alpha：置信度中的系数
theta：置信度中的系数

smartgit测试